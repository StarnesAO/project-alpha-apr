from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from projects.forms import ProjectForm


@login_required
def projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}

    return render(request, "projects/projects.html", context)


# Create your views here.
@login_required
def project_details(request, id):
    project_details = get_object_or_404(Project, id=id)
    context = {
        "project_details": project_details,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
